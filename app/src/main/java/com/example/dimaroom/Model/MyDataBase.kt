package com.example.dimaroom.Model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Info::class), version = 1)
abstract class MyDataBase : RoomDatabase() {
    abstract fun infoDataDAO(): InfoDataDAO

    companion object {
        private var INSTANCE: MyDataBase? = null

        fun getInstance(context: Context): MyDataBase? {
            if (INSTANCE == null) {
                synchronized(MyDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, MyDataBase::class.java, "info.db").build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}