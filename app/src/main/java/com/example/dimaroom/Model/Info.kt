package com.example.dimaroom.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "infoData")
data class Info(@PrimaryKey(autoGenerate = true) var id: String?,
                @ColumnInfo(name = "userName") var userName: String,
                @ColumnInfo(name = "car") var car: String) {
    constructor() : this(null, "", "")

}