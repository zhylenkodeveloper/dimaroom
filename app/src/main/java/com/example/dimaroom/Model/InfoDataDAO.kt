package com.example.dimaroom.Model

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

interface InfoDataDAO {
    @Query("SELECT * from infoData")
    fun getAll(): List<Info>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(infoData: Info)

    @Query("DELETE from infoData")
    suspend fun deleteAll()

}