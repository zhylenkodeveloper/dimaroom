package com.example.dimaroom

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.dimaroom.Model.Info
import com.example.dimaroom.Model.MyDataBase

class MainActivity : AppCompatActivity() {

    private var myDataBase: MyDataBase? = null

    private lateinit var textUserName: TextView
    private lateinit var textUserCar: TextView
    private lateinit var buttonSave: Button
    private lateinit var buttonShowList: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textUserName = findViewById(R.id.name)
        textUserCar = findViewById(R.id.car)
        buttonSave = findViewById(R.id.buttonSave)
        buttonShowList = findViewById(R.id.buttonShow)

        myDataBase = MyDataBase.getInstance(this)

    }

}
